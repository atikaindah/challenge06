const { user_game_biodatas } = require('../models')

class UserBiodatasController {
    static listBiodata(req, res) {
        // select * from tabel
        user_game_biodatas.findAll().then((data) => { res.status(200).json(data) })
            .catch((error) => { res.status(500).json(error) })
    }
}

module.exports = UserBiodatasController