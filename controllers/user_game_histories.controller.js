const { user_game_histories } = require('../models')

class UserHistoriesController {
    static listHistory(req, res) {
        // select * from tabel
        user_game_histories.findAll().then((data) => { res.status(200).json(data) })
            .catch((error) => { res.status(500).json(error) })
    }

}

module.exports = UserHistoriesController