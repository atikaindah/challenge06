const { user_games } = require('../models')

class GamesController {
    static async listUser(req, res) {
        try {
            const user = await user_games.findAll({
                attributes: ['id', 'username'],
                where: {
                    id: req.user.id
                }
            })
            res.status(200).json(user)
          } catch(err) {
            next(err)
          }
        
        // // select * from tabel
        // user_games.findAll({
        //     attributes: ['id', 'username'],
        // }).then((data) => { res.status(200).json(data) })
        //     .catch((error) => { res.status(500).json(error) })
    }

    static async getByIdUser(req, res) {
      try {
        const user = await user_games.findOne({
          where: {
            id: req.params.id
          }
        })

        if (user.id !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          res.status(200).json({
            id: user.id,
            username: user.username,
            
          })
        }
  
      } catch(err) {
        next(err)
      }
        // user_games.findOne({
        //     where: {
        //         id: req.params.id
        //     }
        // }).then((data) => { res.status(200).json(data) })
        //     .catch((err) => { res.status(500).json(err) })
    }

    static async createUser(req, res) {
        try {
            await user_games.create({
              username: req.body.username,
              password: req.body.password,
            })
            res.status(201).json({
              message: 'Succesfully create user'
            })
          } catch (err) {
            next(err)
          }
        // user_games.create({
        //     username: req.body.username,
        //     password: req.body.password,
        // }).then((data) => { res.status(200).json(data) })
        //     .catch((err) => { res.status(500).json(err) })
    }

    static async updateUser(req, res) {
      try {
        await user_games.update(req.body, {
          where: {
            id: req.params.id
          }
        })
        res.status(200).json({
          message: 'Successfully update user'
        })
      } catch(err) {
        next(err)
      }
//         user_games.update({
//             username: req.body.username,
//             password: req.body.password,
//         }, {
//             where: {
//                 id: req.params.id
//             },
//             returning: true,
//         }).then((data) => { res.status(200).json(data[1][0]) })
//             .catch((error) => { res.status(500).json(error) })
    }

    static async deleteUser(req, res) {
      try {
        const user = await user_games.findOne({
          where: {
            id: req.params.id
          }
        })
  
        if (!user) {
          throw {
            status: 404,
            message: 'user not found'
          }
        } else {
          if (user.id !== req.user.id) {
            throw {
              status: 401,
              message: 'Unauthorized request'
            }
          } else {
            await user_games.destroy({
              where: {
                id: req.params.id
              }
            })
            res.status(200).json({
              message: 'Successfully delete user'
            })
          }
        }
      } catch(err) {
        next(err)
      }

//         user_games.destroy({
//             where: {
//                 id: req.params.id
//             }
//         }).then((data) => { res.status(200).json({ message: 'Succesfully delete data' }) })
//             .catch((error) => { res.status(500).json(error) })
//     }
}

}

module.exports = GamesController

