const { user_games } = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

class AuthController {
  static async login(req, res, next) {
    try {
      const user = await user_games.findOne({
        where: {
          username: req.body.username
        }
      })

      if (!user) {
        throw {
          status: 401,
          message: 'Invalid username or password'
        }
      }
      if (bcrypt.compareSync(req.body.password, user.password)) {
        const token = jwt.sign({
          id: user.id,
          username: user.username
        }, 'qwerty')

        res.status(200).json({
          token
        })
      } else {
        throw {
          status: 401,
          message: 'Invalid username or password'
        }
      }
    } catch (err) {
      next(err)
    }
  }
}

module.exports = AuthController