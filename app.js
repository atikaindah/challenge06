const express = require('express')
const app = express()
const port = 3000
const routes = require('./routes')
const errorHandler = require('./errorHandler')
// const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json');

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(routes)
app.use(errorHandler)


// app.use('/api-docs', swaggerUi.serve);
// app.get('/api-docs', swaggerUi.setup(swaggerDocument));

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })

module.exports = app