'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_biodatas.belongsTo(models.user_games, { foreignKey: 'user_game_id', as: 'Id User Game' })
    }
  }
  user_game_biodatas.init({
    name: DataTypes.STRING,
    user_game_id: DataTypes.INTEGER,
    gender: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    age: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_biodatas',
    tableName: 'user_game_biodatas',
  });
  return user_game_biodatas;
};