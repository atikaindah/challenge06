'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_histories.belongsTo(models.user_games, { foreignKey: 'user_game_id', as: 'Id User Game'  })
    }
  }
  user_game_histories.init({
    user_game_id: DataTypes.INTEGER,
    list_time: DataTypes.TIME,
    score: DataTypes.BIGINT
  }, {
    sequelize,
    modelName: 'user_game_histories',
  });
  return user_game_histories;
};