'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
     return queryInterface.bulkInsert('user_game_histories', [
      {
        user_game_id: 1,
        list_time: "09:01:10",
        score: 10921,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_game_id: 2,
        list_time: "11:30:44",
        score: 22560,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_game_id: 3,
        list_time: "15:45:50",
        score: 20901,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_game_id: 4,
        list_time: "23:34:05",
        score: 21782,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_game_id: 5,
        list_time: "10:55:10",
        score: 10801,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_game_histories', null, { truncate: true, restartIdentity: true });
  }
};
