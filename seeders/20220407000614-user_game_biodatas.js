'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('user_game_biodatas', [
      {
        name: "Atika Indah",
        user_game_id: 1,
        gender: "Female",
        date_of_birth: "2001-05-26",
        age: 20,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "William Andrew",
        user_game_id: 2,
        gender: "Male",
        date_of_birth: "2000-10-01",
        age: 21,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Alina Rose",
        user_game_id: 3,
        gender: "Female",
        date_of_birth: "2001-01-10",
        age: 21,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Parker Jospeh",
        user_game_id: 4,
        gender: "Male",
        date_of_birth: "2001-02-10",
        age: 21,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Indah Jasmin",
        user_game_id: 5,
        gender: "Female",
        date_of_birth: "2001-12-12",
        age: 21,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_game_biodatas', null, { truncate: true, restartIdentity: true });

    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
