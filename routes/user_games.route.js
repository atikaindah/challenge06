const express = require("express");
const router = express.Router();
const jwt = require ('jsonwebtoken')
const GamesController = require ('../controllers/user_games.controller')
// const userGames = require('../controllers/user_games.controller');
// const biodatasController = require('../controllers/user_game_biodatas.controller')
// const historiesController = require('../controllers/user_game_histories.controller')

router.use(express.urlencoded({ extended: false }))
router.use(express.json())

router.post('/',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          const user = jwt.decode(req.headers.authorization)
          if (user) {
            req.user = user
            next()
          } else {
            throw {
              status: 401,
              message: 'Unauthorized request'
            }
          }
        }
      } catch (err) {
        next(err)
      }
},

(req, res, next) => {
    const errors = []
    if (!req.body.username) {
      errors.push('Username is required')
    }
    if (!req.body.password) {
      errors.push('Password is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  }, GamesController.createUser
)

router.get('/',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          const user = jwt.decode(req.headers.authorization)
          if (user) {
            req.user = user
            next()
          } else {
            throw {
              status: 401,
              message: 'Unauthorized request'
            }
          }
        }
      } catch (err) {
        next(err)
      }
}, GamesController.listUser
)

router.get('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
}, GamesController.getByIdUser
)

router.put('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
},
(req, res, next) => {
  const errors = []
  if (req.body.username !== undefined) {
    if (!req.body.username) {
      errors.push('Username is required')
    }
  }
  if (req.body.password !== undefined) {
    if (!req.body.password) {
      errors.push('Password is required')
    }
  }

  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
}, GamesController.updateUser
)

router.delete('/:id',
(req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const user = jwt.decode(req.headers.authorization)
      if (user) {
        req.user = user
        next()
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch (err) {
    next(err)
  }
}, GamesController.deleteUser
)

// // List
// router.get('/', userGames.listUser)
// router.get('/biodata', biodatasController.listBiodata)
// router.get('/history', historiesController.listHistory)

// // CRUD user_games
// router.post('/', userGames.createUser)
// router.get('/:id', userGames.getByIdUser)
// router.put('/:id', userGames.updateUser)
// router.delete('/:id', userGames.deleteUser)

module.exports = router;