const express = require('express')
const router = express.Router()
// const userGames = require('../routes/user_games.route')
const authRoutes = require('./auth.route')
const usergamesRoute = require('./user_games.route')


// router.use('/usergames', userGames)
router.use('/login', authRoutes)
router.use('/usergames', usergamesRoute)

module.exports = router