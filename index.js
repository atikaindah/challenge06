const app = require('./app')
const http = require('http')
const port = process.env.PORT

const server = http.createServer(app)

server.listen(port, () => {
  console.log(`App listening on port ${process.env.PORT}`)
})

























// const express = require('express')
// const app = express()
// const port = 3000
// const routes = require('./routes')
// const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json');

// app.use(express.urlencoded({ extended: false }))
// app.use(express.json())

// app.use(routes)
// app.use('/api-docs', swaggerUi.serve);
// app.get('/api-docs', swaggerUi.setup(swaggerDocument));

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })

// module.exports = app