const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require ('jsonwebtoken')
const bcrypt = require('bcryptjs')
const app = require('../app')

let token;

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("5678", salt)
    await queryInterface.bulkInsert('user_games', [
      {
        username: "atikaindah",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    //   {
    //     username: "parkerwilliam",
    //     password: hash,
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },
    //   {
    //     username: "kiranahafsah",
    //     password: hash,
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },
    //   {
    //     username: "alizancurtis",
    //     password: hash,
    //     createdAt: new Date(),
    //     updatedAt: new Date()
    //   },
    ])

    token = jwt.sign({
      id: 1,
      username: 'atikaindah'
    }, 'qwerty')
  
    // token2 = jwt.sign({
    //   id: 2,
    //   username: "parkerwilliam"
    // }, 'qwerty')
  })

  afterEach(async () => {
    await queryInterface.bulkDelete('user_games', {}, { truncate: true, restartIdentity: true })
  })

  describe('POST User', () => {
    it('Success', (done) => {
      request(app)
      .post('/usergames')
      .set("authorization", token)
      .send({
        username: "atikaindah",
        password: "5678",
        // gender: "Female",
        // age: 20,
        // schedule: "2022-06-06",
        // completed: false
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Succesfully create user')
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .post('/usergames')
      .send({
        username: "atikaindah",
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
    it('Invalid auth token', (done) => {
      request(app)
      .post('/usergames')
      .set("authorization", "qwerty123")
      .send({
        username: "atikaindah",
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
   
  })

  describe('GET User', () => {
    it('Success', (done) => {
      request(app)
      .get('/usergames')
      .set('authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(Array.isArray(res.body)).toBe(true)
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .get('/usergames')
      .end((err, res) => {
        if(err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
    it('Invalid token', (done) => {
      request(app)
      .get('/usergames')
      .set('authorization', 'qwerty123')
      .end((err, res) => {
        if(err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
  })

  describe('GET User by ID', () => {
    it('Success', (done) => {
      request(app)
      .get('/usergames/1')
      .set('authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('id')
          expect(res.body).toHaveProperty('username')
          expect(res.body.id).toBe(1)
          expect(res.body.username).toBe('atikaindah')
          done()
        }
      })
    })
    it('No auth', (done) => {
      request(app)
      .get('/usergames/1')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
    it('Invalid token', (done) => {
      request(app)
      .get('/usergames/1')
      .set('authorization', 'qwerty123')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
    
    
  })

  describe('UPDATE /usergames/:id', () => {
    it('Success', (done) => {
      request(app)
      .put('/usergames/1')
      .set('authorization', token)
      .send({
        "username": "atikaindahsari",
        "password": "atika5678"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Successfully update user')
          done()
        }
      })
    })
  
    it('No Auth', (done) => {
      request(app)
      .put('/usergames/1')
      .send({
        "username": "atikaindahsari",
        "password": "atika5678"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
  
    it('Invalid Token', (done) => {
      request(app)
      .put('/usergames/1')
      .set('authorization', 'qwerty123')
      .send({
        "username": "atikaindahsari",
        "password": "atika5678"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
  
    it('Not empty violation', (done) => {
      request(app)
      .put('/usergames/1')
      .set('authorization', token)
      .send({
        "username": "",
        "password": null
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(400)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message.length).toBe(2)
          expect(res.body.message.includes('Username is required')).toBe(true)
          expect(res.body.message.includes('Password is required')).toBe(true)
          done()
        }
      })
    })
  
  })

  describe('DELETE User', () => {

    it('Success', (done) => {
      request(app)
      .delete('/usergames/1')
      .set('authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Successfully delete user')
          done()
        }
      })
    })
  
    it('No auth', (done) => {
      request(app)
      .delete('/usergames/1')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
  
    it('Invalid token', (done) => {
      request(app)
      .delete('/usergames/1')
      .set('authorization', 'qwerty123')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Unauthorized request')
          done()
        }
      })
    })
  
  })


