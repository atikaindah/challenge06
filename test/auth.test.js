const request = require('supertest')
const {sequelize} = require('../models/index')
const { queryInterface } = sequelize
const bcrypt = require('bcryptjs')
const app = require('../app')

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("5678", salt)
    await queryInterface.bulkInsert('user_games', [
        {
            username: "atikaindah",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date()
        }
    ])
})

afterEach(async () => {
    await queryInterface.bulkDelete('user_games', {}, { truncate: true, restartIdentity: true })
})

describe('Login API', () => {
    it('Success', (done) => {
      request(app)
      .post('/login')
      .send({
        username: "atikaindah",
        password: "5678"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('token')
          done()
        }
      })
    })
  
    it('Wrong password', (done) => {
      request(app)
      .post('/login')
      .send({
        username: "atikaindah",
        password: "567"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Invalid username or password')
          done()
        }
      })
    })
  
    it('Wrong username', (done) => {
      request(app)
      .post('/login')
      .send({
        username: "atikaindahsari",
        password: "5678"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Invalid username or password')
          done()
        }
      })
    })
  })